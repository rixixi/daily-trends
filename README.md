# Daily Trends PHP

Descripción

Se pide realizar una pequeña aplicación (DailyTrends) que muestre un feed de noticias. Este
feed es un agregador de noticias de diferentes periódicos. DailyTrends es un periódico que
une las portadas de los periódicos número uno.
Cuando un usuario abre DailyTrends, se encuentra con las 5 noticias de portada de El Pais y
El Mundo del día en el que lo abre, además se pueden añadir noticias a mano desde la
aplicación.

Tareas previas

1. Crear un repositorio de GIT (Bitbucket, GitHub o similar)
2. Antes de empezar las tareas envía el enlace del repositorio a XXXXX.
3. Haz los commits que consideres oportunos conforme vayas desarrollando las
diferentes tareas (Mínimo un commit por tarea).


Tareas a realizar

1. Crea un proyecto con una arquitectura de ficheros básica, gasta un framework si lo
consideras oportuno.

2. Crea un modelo Feed que tenga los atributos: title, body, image, source y publisher.

3. Crea un controlador que se encargue de gestionar a los servicios CRUD del modelo.

4. Crea un “servicio de lectura de feeds” que haga web scraping a cada uno de los
periódicos en busca de su noticia de portada y que la guarde en un Feed.

5. Crear un controlador que gestione los feeds de hoy con su título, descripción,
imagen, fuente y el periódico donde se ha publicado.

6. Crea una vista listado de Feeds que consuman las noticias.

7. Crea una vista detalle de Feed que se pueda editar y borrar.

8. Crea una vista de creación de Feed.


Otros detalles

1. Representa en un dibujo la arquitectura de la aplicación.

2. Usa todas las buenas prácticas que conozcas.

3. Haz los tests que consideres necesarios.

4. Usa el motor de estilos que más te guste.
